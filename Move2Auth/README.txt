Zhang, Jiansong, Zeyu Wang, Zhice Yang, and Qian Zhang. “Proximity Based IoT Device Authentication.” In IEEE INFOCOM 2017-IEEE Conference on Computer Communications, 1–9. IEEE, 2017.

\cite{Zhang2017}