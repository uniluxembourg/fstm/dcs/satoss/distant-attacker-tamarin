Varshavsky, Alex, Adin Scannell, Anthony LaMarca, and Eyal De Lara. “Amigo: Proximity-Based Authentication of Mobile Devices.” In International Conference on Ubiquitous Computing, 253–70. Springer, 2007.

\cite{Varshavsky2007}

This protocol was analised using a Tamarin variant with a richer Diffie-Hellman theory from here https://people.cispa.io/cas.cremers/tools/tamarin/primeorder/index.html.
