Version of Bluetooth SSP Just Works with Distance Bounding, derived from:
Sun, Da-Zhi, Yi Mu, and Willy Susilo. “Man-in-the-Middle Attacks on Secure Simple Pairing in Bluetooth Standard V5. 0 and Its Countermeasure.” Personal and Ubiquitous Computing 22, no. 1 (2018): 55–67.

\cite{Sun2018a}
