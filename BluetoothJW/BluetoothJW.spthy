theory BluetoothJW_xor

begin

builtins: hashing, diffie-hellman, xor



functions: f1/4, f3/7, f2/7, rxor/2

equations: rxor(x, rxor(x, y)) = y, rxor(y, rxor(x, y)) = x


// -------------------- Initialization -------------------- //

rule RegisterAgentHonest:
    [ Fr(~k) ]
  --[ AgentRegistered($X), Once($X), THonest($X)]->
    [ !PublicAgentHonest($X), !PublicAgent($X), !Honest($X), !Ltk($X, ~k) ]	

rule RegisterAgentDishonest:
    [ Fr(~k) ]
  --[ AgentRegistered($X), Once($X), TDishonest($X)]->
    [ !PublicAgent($X), !Dishonest($X), !Ltk($X, ~k) ]	

// -------------------- Special Network Rules ---------------- //

rule TimedSend:
	[ TimedSend($X, id, m) ]--[ StartFast($X, id, m)]->[ Send($X, m), ActiveId($X, id) ]

rule TimedRecv:
    [ ActiveId($X, id), Recv($X, m) ]--[  EndFast($X, id, m) ]->[ TimedRecv($X, id, m) ]
    

rule Send:
	[ Send($X, m) ]--[ TSend($X, m)]->[ !Net(m), Out(m)]

rule Recv:
    [ !Net(m)]--[ TRecv($Y, m) ]->[ Recv($Y, m) ]

// -------------------- Corrupted Agent Rules ---------------- //


rule SendCorrupt:
[ In(m), !Dishonest($X)  ]-->[ Send($X, m) ]

// -------------------- Protocol Rules -------------------- //


rule Initiator_1:
  [ !PublicAgentHonest($A), Fr(~N_A0), Fr(~N_A1), !Ltk($A, ~k) ]
--[ InitiatorStart($A, ~N_A0, ~N_A1, ~k)]->
  [ Out(<'init', <$A, 'g'^~k>>), Initiator1($A, ~N_A0, ~N_A1)]

  // 

rule Responder_1:
  [ Fr(~N_B0), Fr(~N_B1), In(<'init', <A, pkA>>), !PublicAgentHonest($B), !Ltk($B, ~k) ]
--[ ResponderStart($B, ~N_B0, ~N_B1, ~k)]->
  [ Out(<'resp', <$B, 'g'^~k>>), Responder1($B, ~N_B0, ~N_B1, A, pkA) ]

//

rule Initiator_2:
let C_a = f1('g'^~k, pkB, ~N_A0, ~N_A1)
    DHkey = pkB^~k
in
  [ In(<'resp', <B, pkB>>), Initiator1($A, ~N_A0, ~N_A1), !Ltk($A, ~k) ]
--[ ]->
  [ Out(C_a), Initiator2($A, ~N_A0, ~N_A1, B, pkB, DHkey)]

rule Responder_2:
  let C_b = f1('g'^~k, pkA, ~N_B0, ~N_B1)
    DHkey = pkA^~k
  in
  [ In(C_a), Responder1($B, ~N_B0, ~N_B1, A, pkA), !Ltk($B, ~k) ]
-->
  [ Out(C_b), Responder2($B, ~N_B0, ~N_B1, A, pkA, C_a, DHkey)]


//

rule Initiator_3:
  [ In(C_b), Fr(~id), Initiator2($A, ~N_A0, ~N_A1, B, pkB, DHkey) ]
-->
  [ 
    TimedSend($A, ~id, ~N_A0), Initiator3($A, ~id, ~N_A0, ~N_A1, B, pkB, DHkey, C_b) ]

//

rule Responder_3:
  [ In(N_A0), Fr(~id), Responder2($B, ~N_B0, ~N_B1, A, pkA, C_a, DHkey)]
-->
  [ Send($B, rxor(N_A0, ~N_B0)), TimedSend($B, ~id, ~N_B1), Responder3($B, ~id, ~N_B0, ~N_B1, A, pkA, C_a, DHkey, N_A0) ]


// 

rule Initiator_4:
  let 
    //resp = rxor(l, r)
    //N_B0 = ~N_A0 XOR l XOR r
    N_B0 = rxor(~N_A0, resp)
    E_A = f3(DHkey, ~N_A0, ~N_A1, N_B0, N_B1, $A, B) 
    LK = f2(DHkey, ~N_A0, ~N_A1, N_B0, N_B1, $A, B)
  in
  [ TimedRecv($A, ~id, resp), In(N_B1), Initiator3($A, ~id, ~N_A0, ~N_A1, B, pkB, DHkey, C_b), !Ltk($A, ~k)]
--[ Eq(C_b, f1(pkB, 'g'^~k, N_B0, N_B1)),
    RunningA($A, B, LK)]->
  [ Send($A, rxor(N_B1, ~N_A1)), Send($A, E_A), Initiator4($A, ~id, ~N_A0, ~N_A1, B, pkB, DHkey, N_B0, N_B1, resp, LK) ]

rule Responder_4:
  let 
    //resp = rxor(l, r)
    //N_A1 = ~N_B1 XOR l XOR r
    N_A1 = rxor(~N_B1, resp)
    E_B = f3(DHkey, ~N_B0, ~N_B1, N_A0, N_A1, $B, A) 
    LK = f2(DHkey, N_A0, N_A1, ~N_B0, ~N_B1, A, $B)
  in
  [ TimedRecv($B, ~id, resp), In(E_A), Responder3($B, ~id, ~N_B0, ~N_B1, A, pkA, C_a, DHkey, N_A0), !Ltk($B, ~k)]
--[ Eq(C_a, f1(pkA, 'g'^~k, N_A0, N_A1)), 
  Eq(E_A, f3(DHkey, N_A0, N_A1, ~N_B0, ~N_B1, A, $B)),
  SecretB($B, A, LK), TimeMeasurementB($B, ~id, ~N_B1, resp),
  RunningB($B, A, LK), 
  CommitB($B, A, LK),
  EndB($B, A, <N_A0, N_A1, ~N_B0, ~N_B1, LK>) ]->
  [ Send($B, E_B)]


rule Initiator_5:
  [ In(E_B), Initiator4($A, ~id, ~N_A0, ~N_A1, B, pkB, DHkey, N_B0, N_B1, resp, LK)]
--[ Eq(E_B, f3(DHkey, N_B0, N_B1, ~N_A0, ~N_A1, B, $A)),
  SecretA($A, B, LK), TimeMeasurementA($A, ~id, ~N_A0, resp),
  CommitA($A, B, LK), 
  EndA($A, B, <~N_A0, ~N_A1, N_B0, N_B1, LK>)]->
  [  ]

// ------------------- Restrictions -------------------------- //




restriction Once:
"
    All x #t1 #t2.(Once(x) @t1 & Once(x)@t2) ==> #t1 = #t2
"

restriction Eq:
"
    All x y #t.Eq(x, y) @t ==> x = y
"

// -------------------- Sanity Check Lemmas -------------------- //
  

lemma ProtocolRuns:
  exists-trace
"
  Ex A B m #t1 #t2.( 
    EndA(A, B, m)@t2
    & EndB(B, A, m)@t1
    & not(Ex #t E. TDishonest(E)@t)
  )
"


// -------------------- Security Lemmas -------------------- //

lemma SecrecyA:
    all-traces
"
    All A B k id challenge response #t .(
      SecretA(A, B, k)@t
      & TimeMeasurementA(A, id, challenge, response)@t
    ) ==> (
      (
          All  #t1 #t2.(
            StartFast(A, id, challenge)@t1 
            & EndFast(A, id, response)@t2
          )
          ==> (
              not(
                  Ex m E #tm #td.( 
                      TSend(E, m)@tm & #t1 < #tm & #tm < #t2
                      & TDishonest(E)@td
                  )
              )
          )
      )
      ==> 
        not(Ex #t2. K(k)@t2) | (Ex #t2.TDishonest(B)@t2)
    )
"

lemma SecrecyB:
    all-traces
"
    All B A k id challenge response #t .(
      SecretB(B, A, k)@t
      & TimeMeasurementB(B, id, challenge, response)@t
    ) ==> (
      (
          All  #t1 #t2.(
            StartFast(B, id, challenge)@t1 
            & EndFast(B, id, response)@t2
          )
          ==> (
              not(
                  Ex m E #tm #td.( 
                      TSend(E, m)@tm & #t1 < #tm & #tm < #t2
                      & TDishonest(E)@td
                  )
              )
          )
      )
      ==> 
        not(Ex #t2. K(k)@t2) | (Ex #t2.TDishonest(A)@t2)
    )
"


lemma NonInjectiveAgreementA:
all-traces
"
    All A B m id challenge response #t .
      CommitA(A, B, m)@t
      & TimeMeasurementA(A, id, challenge, response)@t
      ==> (
        (
            All  #t1 #t2.(
              StartFast(A, id, challenge)@t1 
              & EndFast(A, id, response)@t2
            )
            ==> (
                not(
                    Ex m E #tm #td.( 
                        TSend(E, m)@tm & #t1 < #tm & #tm < #t2
                        & TDishonest(E)@td
                    )
                )
            )
        )
        ==> 
        (
          Ex #t1.RunningB(B, A, m)@t1 & #t1 < #t
        ) | (Ex #t1.TDishonest(B)@t1)
      )
        
"

lemma NonInjectiveAgreementB:
all-traces
"
  All B A m id challenge response #t .(
    CommitB(B, A, m)@t
    & TimeMeasurementB(B, id, challenge, response)@t
  ) ==> (
    (
        All  #t1 #t2.(
          StartFast(B, id, challenge)@t1 
          & EndFast(B, id, response)@t2
        )
        ==> (
            not(
                Ex m E #tm #td.( 
                    TSend(E, m)@tm & #t1 < #tm & #tm < #t2
                    & TDishonest(E)@td
                )
            )
        )
    )
    ==> 
      (
        Ex #t1.RunningA(A, B, m)@t1 & #t1 < #t
      ) | (Ex #t1.TDishonest(A)@t1)
  )
"


end
/*
==============================================================================
summary of summaries:

analyzed: BluetoothJW.spthy

  ProtocolRuns (exists-trace): verified (18 steps)
  SecrecyA (all-traces): verified (220 steps)
  SecrecyB (all-traces): verified (223 steps)
  NonInjectiveAgreementA (all-traces): verified (183 steps)
  NonInjectiveAgreementB (all-traces): verified (186 steps)

==============================================================================
*/