# Distant Attacker

This repository contains a set of [Tamarin](https://tamarin-prover.github.io) models and (in)security proofs of a number of protocols.

This is the complementary material for our paper *Is Eve nearby? Analysing protocols under the distant-attacker assumption*

## Note

This repository is a copy of the original (https://gitlab.uni.lu/regil/distant-attacker-tamarin)