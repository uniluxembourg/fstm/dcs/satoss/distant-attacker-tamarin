Trujillo-Rasua, Rolando. “Secure Memory Erasure in the Presence of Man-in-the-Middle Attackers.” ArXiv Preprint ArXiv:1905.13474, 2019.

\cite{T2019}