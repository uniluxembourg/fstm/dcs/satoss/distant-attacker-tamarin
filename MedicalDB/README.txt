Rasmussen, Kasper Bonne, Claude Castelluccia, Thomas S. Heydt-Benjamin, and Srdjan Capkun. “Proximity-Based Access Control for Implantable Medical Devices.” In Proceedings of the 16th ACM Conference on Computer and Communications Security, 410–419, 2009.

\cite{Rasmussen2009}
