Singelee, Dave, and Bart Preneel. “Key Establishment Using Secure Distance Bounding Protocols.” In 2007 Fourth Annual International Conference on Mobile and Ubiquitous Systems: Networking & Services (MobiQuitous), 1–6. IEEE, 2007.

\cite{SP2007}