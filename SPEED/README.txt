Ammar, Mahmoud, Wilfried Daniels, Bruno Crispo, and Danny Hughes. “Speed: Secure Provable Erasure for Class-1 Iot Devices.” In Proceedings of the Eighth ACM Conference on Data and Application Security and Privacy, 111–118, 2018.

\cite{ADCH2018}